<%@ include file="menu-header.jsp"%>

<form:form action="${webappRoot}secure/submit-password-change"
	method="POST" commandName="changePass">


	<form:label path="oldPassword">Old Password</form:label>
	<form:password path="oldPassword" />
	<form:errors path="oldPassword" cssClass="error" />

	<br />
	<form:label path="newPassword">New Password</form:label>
	<form:password path="newPassword" />
	<form:errors path="newPassword" cssClass="error" />
	<br />
	<form:label path="confirmation">confirmation</form:label>
	<form:password path="confirmation" />
	<form:errors path="confirmation" cssClass="error" />
	<br />
	<input type="submit" class="btn btn-default" value="Change Password" />

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

</form:form>

<%@ include file="menu-footer.jsp"%>