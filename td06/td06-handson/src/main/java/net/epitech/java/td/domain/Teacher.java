package net.epitech.java.td.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Teacher database table.
 * 
 */
@Entity
@NamedQuery(name="Teacher.findAll", query="SELECT t FROM Teacher t")
public class Teacher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String mail;

	private String name;

	private String pwd;

	//bi-directional many-to-one association to Course
	@OneToMany(mappedBy="teacher", fetch=FetchType.EAGER)
	private List<Course> courses;

	//bi-directional many-to-many association to Access
	@ManyToMany
	@JoinTable(
		name="Teacher_has_Access"
		, joinColumns={
			@JoinColumn(name="teacherId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="accessId")
			}
		)
	private List<Access> accesses;

	public Teacher() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public List<Course> getCourses() {
		return this.courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public Course addCours(Course cours) {
		getCourses().add(cours);
		cours.setTeacher(this);

		return cours;
	}

	public Course removeCours(Course cours) {
		getCourses().remove(cours);
		cours.setTeacher(null);

		return cours;
	}

	public List<Access> getAccesses() {
		return this.accesses;
	}

	public void setAccesses(List<Access> accesses) {
		this.accesses = accesses;
	}

}