package net.epitech.java.td.repositories.custom;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import net.epitech.java.td.domain.QCourse;
import net.epitech.java.td.domain.QTeacher;
import net.epitech.java.td.domain.Teacher;

import org.springframework.stereotype.Component;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.jpa.impl.JPAQuery;
/**
 * JPA based implementation.
 * @author nicolas
 *
 */
@Component
public class TeacherRepositoryCustomImpl implements TeacherRepositoryCustom {

	@Inject
	EntityManagerFactory emFactory;

	@Override
	public List<Teacher> getUnemployedTeachers() {
		EntityManager em = emFactory.createEntityManager();
		try {
			JPQLQuery query = new JPAQuery(em);
			QTeacher teacher = QTeacher.teacher;
			QCourse course = QCourse.course;
			List<Teacher> res = query.from(teacher)
					.leftJoin(teacher.courses, course)
					.groupBy(teacher.id)
					.having(course.count().eq(0L))
					.list(teacher);
			return res;
		} finally {
			em.close();
		}
	}

}
