<%@ include file="menu-header.jsp"%>

<c:forEach var="cours" items="${courses}">
	<ol class="breadcrumb">
		<li><c:out value="${cours.name}"></c:out></li>
		<li><fmt:formatDate value="${cours.date.toDate()}" pattern="EEEE" /></li>
		<li><c:out value="${cours.duration.getStandardHours()} h"></c:out></li>
	</ol>
</c:forEach>

<%@ include file="menu-footer.jsp"%>