package net.epitech.java.td.repositories.custom;

import java.util.List;

import net.epitech.java.td.domain.Teacher;

/**
 * extra access for teacher repo, where we can't do that in querydsl.
 * @author nicolas
 *
 */
public interface TeacherRepositoryCustom {
	
	public List<Teacher> getUnemployedTeachers();

}
