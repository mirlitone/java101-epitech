package net.epitech.java.td.service.impl;

import static org.junit.Assert.fail;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.InvalidPasswordException;
import net.epitech.java.td.exception.NoSuchTeacherException;
import net.epitech.java.td.repositories.TeacherRepositoy;

import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EpitechServiceTest {

	@Test
	public void test() {
		EpitechServiceImpl service = new EpitechServiceImpl();

		// create objects to test
		Teacher nicolas = new Teacher();
		nicolas.setMail("nicolas@mirlitone.com");
		nicolas.setPwd("toto");

		// configurer les repository avec easymock
		TeacherRepositoy repo = EasyMock.createNiceMock(TeacherRepositoy.class);
		EasyMock.expect(repo.findByMail(EasyMock.eq(nicolas.getMail())))
				.andReturn(nicolas).anyTimes();
		EasyMock.expect(repo.saveAndFlush(EasyMock.anyObject(Teacher.class)))
				.andReturn(null).anyTimes();
		EasyMock.replay(repo);

		// configure password encoder
		PasswordEncoder encoder = EasyMock
				.createNiceMock(PasswordEncoder.class);
		EasyMock.expect(
				encoder.matches(EasyMock.eq("toto"), EasyMock.eq("toto")))
				.andReturn(Boolean.TRUE).anyTimes();
		EasyMock.expect(
				encoder.matches(EasyMock.anyString(), EasyMock.anyString()))
				.andReturn(Boolean.FALSE).anyTimes();

		EasyMock.expect(encoder.encode(EasyMock.eq("toto"))).andReturn("toto")
				.anyTimes();
		EasyMock.replay(encoder);

		service.passwordEncoder = encoder;
		service.teacherRepo = repo;

		try {
			service.changePassword("nicolas@mirlitone.com", "titi", "toto");
		} catch (NoSuchTeacherException | InvalidPasswordException e) {
			fail("should not have thrown");
		}

		try {
			service.changePassword("nicolas@mirlitone.com", "tata", "titi");
			fail("should have thrown");
		} catch (NoSuchTeacherException e) {
			fail("should not have thrown");
		} catch (InvalidPasswordException e) {

		}

		try {
			service.changePassword("nicolas2@mirlitone.com", "tata", "titi");
			fail("should not have thrown");
		} catch (NoSuchTeacherException e) {

		} catch (InvalidPasswordException e) {
			fail("should not have thrown");
		}

	}

}
