package net.epitech.java.td.exception;

/**
 * exception thrown with mail is not valid according to hibernate validation.
 * @author nicolas
 *
 */
public class FailedToParseMailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5314445716669743030L;

}
