#�TD7 : S�curit� #


## Objectifs du TD ##

### D�mo ###

Correction TD06


### Techniques ###

+ spring security
+ authentication vs authorization
+ Roles

S�curit�
================

Vocabulaire
--------------

+ Authentication : Qui je suis ?
+ Authorisation: A quoi ai-je droit d'acc�der
+ Roles, Authority: Affect� � un utilisateur, le r�le est un ensemble de droit d'acc�s. Il peut y avoir plusieurs R�les par utilisateur
+ Principal: repr�sente de mani�re abstraite un utilisateur du syst�me de s�curit� (utilisateur, machine, corporation...)


Que faut-il s�curiser dans une application web?
------------------------------------------------

+ La s�curit� est le probl�me n� des applications web (avant la performance, l'ergonomie, le design...)

###�Les trois grands type d'utilisateurs ###

+ Les utilisateurs anonymes: ils ne sont pas connect�s, ce sont des utilisateurs d'internet lambda
+ Les utilisateurs authentifi�s: ils sont reconnu par le syst�me � l'aide d'un m�canisme d'authentificaiont (login/mdp, CAS, X 509)
+ Les utilisateurs administrateur: ils ont les droits qui sont r�serv�s aus m�mbres de l'organisation qui g�re le site.

### Quels sont les �l�ments fonctionnels � s�curiser? ###

+ Vue d'avion: les liens (Vue)
  + dans vos pages JSP, il ne faut pas montrer un lien auquel l'utilisateur n'a pas acc�s.
  + utilisation de la taglib <sec:

+ Vue macro: les URLS (Controlleur)
  + par exemple l'url /secure peut n'�tre accessible qu'aux utilisateurs 
  + par exemple l'url /admin peut n'�tre accessible qu'aux administrteurs
  
+ Vue pr�cise: les services (Mod�le)
  + postulat: on ne fait pas confiance aux autres programmeurs qui vont utiliser l'API de service.
  + exemple: les services de lecture (get, find...) sont accessibles � tout le monde
  + exemple: les services de modification (delete, update) sont accessible qu'aux admins

###�Quels sont les �l�ments techniques � s�curiser? ###

+ (void doc spring)[http://docs.spring.io/spring-security/site/docs/3.2.1.RELEASE/reference/htmlsingle/#hello-web-security-java-configuration]
+ CSRF attack prevention
+ Clickjacking
+ Cache Control

### Question bonus ###

+ comment stocker les mots de passe de mani�re s�curis�?
  

Comment est configur�e la s�curit� dans les applications spring-security?
--------------------------------------------------------------------------

### Les d�pendances Maven ###

-----------------------------

        <!-- security -->
            <dependency>
              <groupId>org.springframework.security</groupId>
              <artifactId>spring-security-web</artifactId>
              <version>3.2.1.RELEASE</version>
            </dependency>
            <dependency>
              <groupId>org.springframework.security</groupId>
              <artifactId>spring-security-config</artifactId>
              <version>3.2.1.RELEASE</version>
            </dependency>
            <dependency>
              <groupId>org.springframework.security</groupId>
              <artifactId>spring-security-taglibs</artifactId>
              <version>3.2.1.RELEASE</version>
            </dependency>

------------------------------

###�la plomberie/Configuration dans spring ###

+ depuis la version 3.1, spring-security utilise l'API servlet 3.0, ce qui signifie que l'on peut �crire le contenu du fichier web.xml enti�rement en JAVA
+ une classe qui h�rite de WebApplicationInitializer doit �tre d�finie dans le contexte: elle sera charg�e par spring (ici net.epitech.java.td.config.MessageWebApplicationInitializer)
+ une classe doit enregistrer la s�curit� (ici net.epitech.java.td.config.security.MessageSecurityWebApplicationInitializer)
+ Les beans de s�curit� sont d�finis dans une classe annot�e avec @Configuration (ici net.epitech.java.td.config.security.SecurityConfig)
+ Le m�canisme d'authentification doit �tre configur� dans une class qui �tend UserDetailsService  (net.epitech.java.td.config.security.EpitechUserDetailService). Ici cette classe utilise les m�canismes de persistance que nous avons d�j� mis en place (repositories)

### V�rifier l'authentificaiton des utilisateurs ###

+ On va �tendre UserDetails Services


------------------------

        @Transactional
          // @Transactional annotation is used to access lazy initialization for
          // domain object
          @Override
          public UserDetails loadUserByUsername(String mail)
              throws UsernameNotFoundException {

            // find a teacher by mail
            Teacher teachers = teacherRepo.findByMail(mail);
            if (teachers == null) {
              // bad credentials
              throw new UsernameNotFoundException(mail);
            }

            // from the Access table, we get roles as strings and convert them as
            // Granted Authority, using Guava
            Collection<GrantedAuthority> accessAsGrantedAuthority = Lists
                .newArrayList(Iterables.transform(teachers.getAccesses(),
                    new Function<Access, GrantedAuthority>() {

                      @Override
                      public GrantedAuthority apply(Access access) {
                        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(
                            access.getName());
                        return authority;

                      }

                    }));

            // plug user info and well as authorities
            User user = new User(teachers.getMail(), teachers.getPwd(),
                accessAsGrantedAuthority);
            return user;
          }
  
------------------------------

### Dans la vue ###

+ on ajoute la taglib de spring security

---------------------------
        <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

---------------------------

+ puis on l'utilise ainsi:

---------------------------
        <sec:authorize ifAllGranted="ADMIN"> ... </sec:authorize>
        
---------------------------

+ lors de la soumission de formulaire, on doit ajouter le token de s�curit�:

--------------------

        <input type="hidden" name="${_csrf.parameterName}"
              value="${_csrf.token}" />
      
------------------

### Acc�s au niveau URL ###
+ configur� dans net.epitech.java.td.config.security.SecurityConfig

---------------------
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
            .antMatchers("/resources/**").permitAll()
            .antMatchers("/teacher**").hasAuthority("ADMIN")
            .antMatchers("/cours**")
            .anonymous().anyRequest().authenticated().and().formLogin();
          }

----------------------

###�Acc�s au niveau Service ###

+ l'activation de cette fonctionnalit� se faire via l'annotation de la classe net.epitech.java.td.config.security.SecurityConfig


-----------------------
        @EnableGlobalMethodSecurity(prePostEnabled = true)  

-----------------------

+ Elle se fait ensuite au niveau du service � l'aide des annotations @Preauthorize


-----------------------
        @PreAuthorize("hasRole('ADMIN')")
          public Collection<Teacher> getTeacherWithNoCourses();

------------------------


A vous de jouer!
==================

Lot 1 
--------

Touvez les failles de s�curit�!

Lot 2
-------

R�parer les liens qui ne devraient pas �tre accessibles

Lot 3
-------

Architecturer les URLS du controller pour �tre plus simple � s�curiser












