package net.epitech.java.td01.td0ioc.model.inj;

import javax.inject.Inject;
import javax.inject.Named;

import net.epitech.java.td01.td0ioc.model.AbstractTeachable;
import net.epitech.java.td01.td0ioc.model.contract.Nameable;
import net.epitech.java.td01.td0ioc.model.contract.Teachable;
import net.epitech.java.td01.td0ioc.model.types.CourseType;

public class JavaCourse extends AbstractTeachable implements Teachable {
	
	@Inject
	@Named("java")
	private Nameable teacher;

	public CourseType getCourse() {
		return CourseType.JAVA;
			
	}

	public Nameable getTeacher() {
		return this.teacher;
	}

	public String getCourseMaterial() {
		return "everything is object";
	}

}
