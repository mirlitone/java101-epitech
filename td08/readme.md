# TD8 : Qualité #


## Objectifs du TD ##

### Techniques ###

+ controler la qualité logicielle 
+ point sur les tests unitaires


### Questions/Réponses ###

* périmètre du projet
* aide sur vos devs sur le projet epimarket

### EPIMARKET ###

#### technical requirements ####

- 100% technos web. Utilisation obligatoire d'un framework MVC.
- Gérer du reporting (par exemple les commandes, ou bien les stats).
- Utiliser un ORM.
- Avoir un modèle de données clair et coordonné avec les problématiques métiers !
- Doit être compatible avec la majorité des browsers du marché. (Firefox, Chrome)
- Doit gérer les sessions users, un panier virtuel, une phase de login avec des rôles, une interface d’administration, ...
- Faire passer un checkstyle et avoir 0 erreur.
- écrire des tests unitaires:
  - couverture: <=5,00%  ==> -20% sur la note finale
  - couverture:  5,00-20,00%  ==> OK
  - couverture:  >20,00%  ==> +20% sur la note
- pull request sur mon repo bb?
- développer selon les bonnes pratiques vues en cours avec Nicolas + toutes les excellentes remarques qui sont faites sur les cours vidéo.


#### Usecases ####

* en tant qu'utilisateur anonyme, je peux parcourir la liste des produits.
* en tant qu'utilisateur anonyme, je peux créer mon compte sur le site
* en tant qu'utilisateur connecté, je peux passer une commande à partir d'une selection d'articles sur le site
* en tant qu'utilisateur connecté, je peux visualisé l'avancée d'une commande.
* en tant qu'utilisateur connecté, je peux modifier les informations de mon compte
* en tant qu'administrateur du système, je peux ajouter, supprimer et modifier les produits disponibles
* en tant qu'administrateur du système, je peux accéder à un outil de reporting de mes commandes.





# Qualité logicielle #


## Qualité fonctionnelle ##

+ est-ce que le logiciel correspond bien aux besoins?
    + Le chef produit est garant de la réponse globale au besoin
    + l'équipe QA est responsable de détecter les anomalies et d'en fournir la liste au chef produit qui décide quoi en faire.
+ quelles sont les anomalies connues du logicielle
    + les anomalies connues sur le fruit d'arbitrages techniques et fonctionnels:
    + ex. le site ne fonctionne pas sous IE6. C'est une anomalie connue et la réponse est "Le client doit utiliser un navigateur compatible, dont on fourni une liste"
    + ex. le site ne supporte pas plus de 1.000.000 de connections simultannée. La réponse est : le système doit indiquer aux utilisateurs que le site recontre des difficultés liées à une saturation et invite les clients à se reconnecter ultèrieurement
  
+ est-ce que le logiciel comporte des bugs?
    + c'est une annomalie insoupsonnée, remonté par QA ou par le support client
    + les bugs doivent êtres indiqués dans l'outil de bugtracking et soumis à arbitrage du chef produit
  
## Qualité fonctionnelle ###

+ elle correspond à des besoins non exprimés par les utilisateurs ou le chef produit:
    + accessibilité
    + sauvegarde
    + tolérance aux pannes
    + maintenabilité
  
## Qualité technique ##

+ elle est la responsabilité de la direction technique, souvent d'un Architecte

### Maintenabilité ###

+ l'architecte peut imposer des critères de qualité du code
    + nombre de ligne
    + complexité cyclomatique
    + mesures de McCabe
    + couverture de test
  
#### Mesure statiques ####

+ rien qu'en lisant le code, on peut voir s'il respecte le "State of the Art" de la profession

#### Coding convention #####

+ nomage, indentation, gestion des erreurs
+ code convention d'Oracle http://www.oracle.com/technetwork/java/codeconventions-150003.pdf
+ Apache Maven Checkstyle Plugin pour le faire tourner, dans la ligne faites:

------------------------------
    mvn checkstyle:checkstyle
    
------------------------------
    
+ ouvrir target/site/checkstyle.html pour avoir le rapport
+ le checkstyle fourni doit être utilisé pour la présentation, et il ne doit pas y avoir d'erreur.

##### Coding metrics #####

+ les métriques de codage vous indiquer lorsque vos fonctions sont trop complexes ou insuffisament couverte par les tests unitaires.
+ Démo de Sonar
+ sonar peut être lancé à l'aide de la commande:

----------------------------
    mvn sonar:sonar

-----------------------------

+ après avoir lancé le serveur.
